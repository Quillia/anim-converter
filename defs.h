// might be cleaner to just use arrays. . .
struct vector3
{	float x,y,z;
};
struct quat
{	float x,y,z,s;
};
// rotational axes and positional movement axes.
enum axis_spec{ a_X, a_Y, a_Z, m_X, m_Y, m_Z };
struct quat euler2rot(float a, float b, float c, enum axis_spec a_a, enum axis_spec a_b, enum axis_spec a_c);
char* bone2bone(const char* bone);
