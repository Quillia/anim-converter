#include "defs.h"
// for debug only:
//#include <stdio.h>
//#include <math.h>
double sin(double x);
double cos(double x);
#define M_PI 3.14159265358979323846
double DEG_TO_RAD = (2*M_PI)/360.0;

struct quat aa2q(enum axis_spec axis, float angle)
{	struct quat ret;
	ret.x=0; ret.y=0; ret.z=0; ret.s=-1;
	// we tacitly switch axes here to conform to linden standard
	// LINDEN: z=up, x=fwd, y=left.
	// BVH:    y=up, x=left, z=fwd
	// --> y->z x->y z->x
	switch(axis)
	{	case a_X:
			ret.y = sin(angle*0.5*DEG_TO_RAD);
		break;
		case a_Y:
			ret.z = sin(angle*0.5*DEG_TO_RAD);
		break;
		case a_Z:
			ret.x = sin(angle*0.5*DEG_TO_RAD);
		break;
	}
	ret.s = cos(angle*0.5*DEG_TO_RAD);
	//printf("angle : %f ",angle);
	//printf("quaternion: %f %f %f %f\n",ret.x,ret.y,ret.z,ret.s);
	return ret;
}
struct quat quatMul(struct quat a, struct quat b)
{	// math order. not linden order.
	struct quat ret;
	ret.x = (a.s*b.x) + (-1*a.z*b.y) + (a.y*b.z) + (a.x*b.s);
	ret.y = (a.z*b.x) + (a.s*b.y) + (-1*a.x*b.z) + (a.y*b.s);
	ret.z = (-1*a.y*b.x) + (a.x*b.y) + (a.s*b.z) + (a.z*b.s);
	ret.s = (-1*a.x*b.x) + (-1*a.y*b.y) + (-1*a.z*b.z) + (a.s*b.s);
	return ret;
}
struct quat euler2rot(
	float a, float b, float c,
	enum axis_spec a_a,
	enum axis_spec a_b,
	enum axis_spec a_c )
{	return	quatMul(quatMul(aa2q(a_a,a),aa2q(a_b,b)),aa2q(a_c,c));
}
