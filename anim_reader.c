/*
 * Done:
 * 	IN: anim.
 * 	OUT: txtanim, anim
 *
 * Todo:
 * 	combine x,y,z into one line.
 *
 * */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <string.h>
#include "defs.h"

enum Filetype{file_anim, file_bvh, file_txtanim};
enum Datatype{data_String, data_String16, data_U8, data_U16, data_U32, data_S8, data_S16, data_S32, data_F32, data_V3};

#define VERBOSE 0

// from llquantize.h
const uint16_t U16MAX = 65535;
const float OOU16MAX = 1.0/(float)(U16MAX);

uint16_t F32_to_U16(float val, float lower, float upper)
{	if(val<lower) val = lower;
	else if(val>upper) val = upper;
	val-= lower;
	val /= (upper-lower);
	return (uint16_t)round(val*U16MAX); // LL uses floor, but round is less lossy. (in.anim==out.anim)
}
float U16_to_F32(uint16_t ival, float lower, float upper)
{	float val = ival*OOU16MAX;
	float delta = (upper-lower);
	val*=delta;
	val+=lower;

	float max_error = delta*OOU16MAX;
	if(fabs(val) < max_error)
		val = 0.0;
	return val;
}

struct anim // 
{	uint16_t version;
	uint16_t subversion;
	int32_t  priority;
	float    duration;
	char*    name;
	float    loop_in;
	float	 loop_out;
	int32_t  loop;
	float    ease_in;
    float    ease_out;
    uint32_t hand_pose;
	uint32_t n_joints;
	struct joint_data *joints;
	uint32_t n_constraints;
	struct constraint_data *constraints;
} ;
struct joint_data
{	char* name;
	int32_t priority;
	int32_t n_rot_keys;
	struct rot_data* rots; 
	int32_t n_pos_keys;
	struct pos_data* posns;
};
struct rot_data
{	float time; // all uInt16's as packed in .anim.
	float rot_x;
	float rot_y;
	float rot_z;
	//float rot_s;
};
struct pos_data
{	float time; // all uint16's as packed in .anim.
	float pos_x;
	float pos_y;
	float pos_z;
};
struct constraint_data
{
	uint8_t chain_length;
	uint8_t constraint_type;
	char source_volume[17]; // 16+ a definitive terminating null byte.
	struct vector3 source_offset;
	char target_volume[17];
	struct vector3 target_offset;
	struct vector3 target_dir;
	float ease_in_start;
	float ease_in_stop;
	float ease_out_start;
	float ease_out_stop;
};


size_t read(enum Datatype typeData, void* value, const char* name, FILE* fp, enum Filetype typeFile)
{
	switch(typeData)
	{
		case data_U8:
		switch(typeFile)
		{	case file_anim:
				return fread(value, 1, 1, fp);
			case file_txtanim:
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				size_t ret = fscanf(fp,"%s %hhu\n",n,(uint8_t*)value);
				printf("'%s' found, expecting: '%s:' \n",n,name);
				printf("'%hhu' read.\n",*(uint8_t*)value);
				return ret;
			}
			#endif
				return fscanf(fp,"%*s %hhu\n",(uint8_t*)value);
		}
		break; // break superfluous because of returns.
		case data_U16:
		switch(typeFile)
		{	case file_anim:
			return fread(value, 2, 1, fp);
			break;
			case file_txtanim:
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				size_t ret = fscanf(fp,"%s %hu\n",n,(uint16_t*)value);
				printf("'%s' found, expecting: '%s:' \n",n,name);
				printf("'%hu' read.\n",*(uint16_t*)value);
				return ret;
			}
			#endif
			return fscanf(fp,"%*s %hu\n",(uint16_t*)value);
			break;
		}
		break;
		case data_U32:
		switch(typeFile)
		{	case file_anim:
				return fread(value, 4, 1, fp);
			case file_txtanim:
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				size_t ret = fscanf(fp,"%s %u\n",n,(uint32_t*)value);
				printf("'%s' found, expecting: '%s:' \n",n,name);
				printf("'%u' read.\n",*(uint32_t*)value);
				return ret;
			}
			#endif
				return fscanf(fp,"%*s %u\n",(uint32_t*)value);
		}
		break;
		case data_S8:
		switch(typeFile)
		{	case file_anim:
				return fread(value, 1, 1, fp);
			case file_txtanim:
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				size_t ret = fscanf(fp,"%s %hhi\n",n,(int8_t*)value);
				printf("'%s' found, expecting: '%s:' \n",n,name);
				printf("'%hhi' read.\n",*(int8_t*)value);
				return ret;
			}
			#endif
				return fscanf(fp,"%*s %hhi\n",(int8_t*)value);
			break;
		}
		break;
		case data_S16:
		switch(typeFile)
		{	case file_anim:
				return fread(value, 2, 1, fp);
			case file_txtanim:
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				size_t ret = fscanf(fp,"%s %hi\n",n,(int16_t*)value);
				printf("'%s' found, expecting: '%s:' \n",n,name);
				printf("'%hi' read.\n",*(int16_t*)value);
				return ret;
			}
			#endif
				return fscanf(fp,"%*s %hi\n",(int16_t*)value);
			break;
		}
		break;
		case data_S32:
		switch(typeFile)
		{	case file_anim:
				return fread(value, 4, 1, fp);
			case file_txtanim:
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				size_t ret = fscanf(fp,"%s %i\n",n,(int32_t*)value);
				printf("'%s' found, expecting: '%s:' \n",n,name);
				printf("'%i' read.\n",*(int32_t*)value);
				return ret;
			}
			#endif
				return fscanf(fp,"%*s %i\n",(int32_t*)value);
			break;
		}
		break;
		case data_F32:
		switch(typeFile)
		{	case file_anim:
				return fread(value, 4, 1, fp);
			case file_txtanim:
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				size_t ret = fscanf(fp,"%s %f\n",n,(float*)value);
				printf("'%s' found, expecting: '%s:' \n",n,name);
				printf("'%f' read.\n",*(float*)value);
				return ret;
			}
			#endif
				return fscanf(fp,"%*s %f\n",(float*)value);
			break;
		}
		break;
		case data_V3:
		switch(typeFile)
		{	case file_anim:
				return fread(value, 12, 1, fp);
			case file_txtanim:
				return fprintf(fp,"%*s %f, %f, %f\n",name,
					&(((struct vector3*)value)->x),
					&(((struct vector3*)value)->y),
					&(((struct vector3*)value)->z)
				);
			break;
		}
		break;
		case data_String:
		switch(typeFile)
		{	case file_anim:
			// this is probbably really bad form:
			char* buffer = value;
			int index = 0;
			do { 
				fread(buffer+index, 1,1,fp);
				// debug: //
				#ifdef VERBOSE
				if(VERBOSE) printf("'%c'",buffer[index]);
				#endif
			}while(buffer[index++]);
				#ifdef VERBOSE
				if(VERBOSE) printf("\n");
				#endif
			buffer[index]=0; // maybe guard against null string weirdness?
			return index;
			break;
			case file_txtanim:
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				fscanf(fp,"%s",n);
				printf("'%s' found, expecting: '%s:' \n",n,name);
			}else
			#endif
			{	fscanf(fp,"%*s");
			}
			char* buf = value;
			size_t index_buf=0;

			// hopefully, consume exactly 1 space.
			fread(&buf[index_buf],1,1,fp);

			do{ fread(&buf[index_buf],1,1,fp);
			}while(buf[index_buf++]!='\n');

			buf[--index_buf] = '\0';
			#ifdef VERBOSE
			if(VERBOSE) printf("read string: %s\n",buf);
			#endif
			return index_buf;
		}
		break;
		case data_String16:
		switch(typeFile)
		{	case file_anim:
			// this is probbably really bad form:
			char* buffer = value;
			int index = 0;
			do { 
				fread(buffer+index, 1,1,fp);
			}while(index<16);
			buffer[16] = 0;
			return index;
			break;
			case file_txtanim:
// TODO major: string read for txtanim must handle null string and names with space. reccomend attempt read until '\n'.
			#ifdef VERBOSE
			if(VERBOSE)
			{	char n[32];
				size_t ret = fscanf(fp,"%s %hd\n",n,(uint16_t*)value);
				printf("'%s' found, expecting: '%s:' \n",n,name);
				printf("'%hu' read.\n",*(uint16_t*)value);
				return ret;
			}
			#endif
			return fscanf(fp,"%*s %s",(char*)value);
		}
		break;
	}
	return 0;
}

size_t write(enum Datatype typeData, void* value, const char* name, FILE* fp, enum Filetype typeFile)
{
	switch(typeData)
	{
		case data_U8:
		switch(typeFile)
		{	
			case file_anim:
			return fwrite(value,1,1,fp);
			break;
			case file_txtanim:
			return fprintf(fp,"%s: %hhd\n",name,*(char*)value);
			break;
		}
		break;
		case data_U16:
		switch(typeFile)
		{
			case file_anim:
			return fwrite(value,2,1,fp);
			break;
			case file_txtanim:
			return fprintf(fp,"%s: %hd\n",name,*(uint16_t*)value);
			break;
		}
		break;
		case data_U32:
		switch(typeFile)
		{	
			case file_anim:
			return fwrite(value,4,1,fp);
			break;
			case file_txtanim:
			return fprintf(fp,"%s: %u\n",name,*(uint32_t*)value);
			break;
		}
		break;
		case data_S8:
		switch(typeFile)
		{	
			case file_anim:
			return fwrite(value,1,1,fp);
			break;
			case file_txtanim:
			return fprintf(fp,"%s: %hhd\n",name,*(int8_t*)value);
			break;
		}
		case data_S16:
		switch(typeFile)
		{	
			case file_anim:
			return fwrite(value,2,1,fp);
			break;
			case file_txtanim:
			return fprintf(fp,"%s: %hd\n",name,*(int16_t*)value);
			break;
		}
		case data_S32:
		switch(typeFile)
		{	
			case file_anim:
			return fwrite(value,4,1,fp);
			break;
			case file_txtanim:
			return fprintf(fp,"%s: %d\n",name,*(int32_t*)value);
			break;
		}
		break;
		case data_F32:
		switch(typeFile)
		{	
			case file_anim:
			return fwrite(value,4,1,fp);
			break;
			case file_txtanim:
			return fprintf(fp,"%s: %f\n",name,*(float*)value);
			break;
		}
		break;
		case data_String:
		switch(typeFile)
		{
			case file_anim:
			char* str = value;
			int indexChar=0;
			do {
				fwrite(&str[indexChar],1,1,fp);
			} while(str[indexChar++]);
			break;

			case file_txtanim:
			return fprintf(fp,"%s: %s\n",name,(char*)value);
		}
		break;
		case data_String16:
		switch(typeFile)
		{
			case file_anim:
			return fwrite(value,1,16,fp);
			break;
			case file_txtanim:
			return fprintf(fp,"%s: %s\n",name,(char*)value);
			
		}
		break;
		case data_V3:
		switch(typeFile)
		{
			case file_anim:
			return fwrite(value,12,1,fp);
			break;
			case file_txtanim:
			struct vector3 *v3 = value;
			return fprintf(fp,"%s: %f, %f, %f\n",name,v3->x,v3->y,v3->z);
			
		}
		break;
	}
	return 0;
}


int read_in_anim(struct anim* anim, FILE* fp,enum Filetype type)
{
	switch(type)
	{
		case file_anim:
		case file_txtanim:
			read(data_U16,&anim->version,"version",fp,type);
			read(data_U16,&anim->subversion,"subversion",fp,type);
				// check that version ==1, subversion == 0
			read(data_S32,&anim->priority,"base_priority",fp,type);
			read(data_F32,&anim->duration,"duration",fp,type);
				// check against maximum duration.
			anim->name = malloc(32);
			read(data_String,anim->name,"emote_name",fp,type);
			#ifdef VERBOSE
			if(VERBOSE) printf("%s \n",anim->name);
			#endif
			read(data_F32,&anim->loop_in,"loop_in_point",fp,type);
			read(data_F32,&anim->loop_out,"loop_out_point",fp,type);
			// minor hack:
			//	if(anim->loop_out > anim->duration) anim->duration = anim->loop_out;
			read(data_S32,&anim->loop,"loop?",fp,type);
			read(data_F32,&anim->ease_in,"ease_in_duration",fp,type);
			read(data_F32,&anim->ease_out,"ease_out_duration",fp,type);
			read(data_U32,&anim->hand_pose,"hand_pose",fp,type);
			read(data_U32,&anim->n_joints,"num_joints",fp,type);
				// must be at least 1 and less than LL_CHARACTER_MAX_ANIMATED_JOINTS.
			// reserve memory for the joints.
			anim->joints = malloc(anim->n_joints*sizeof(struct joint_data));
			for(int indexJoint=0; indexJoint<(anim->n_joints); ++indexJoint)
			{
				anim->joints[indexJoint].name = malloc(32);
				read(data_String, anim->joints[indexJoint].name, "\tjoint_name",fp,type);

				read(data_S32, &anim->joints[indexJoint].priority, "\tjoint_priority",fp,type);
				read(data_S32, &anim->joints[indexJoint].n_rot_keys, "\tnum_rot_keys",fp,type);

				// reserve memory for rotations
				anim->joints[indexJoint].rots = malloc(anim->joints[indexJoint].n_rot_keys*sizeof(struct rot_data)) ;
				
				switch(type)
				{	case file_anim:
					for(int indexRotKey=0; indexRotKey<anim->joints[indexJoint].n_rot_keys ; ++indexRotKey)
					{	
						uint16_t time_short;
						read(data_U16,&time_short,"\t\ttime",fp,type);
						anim->joints[indexJoint].rots[indexRotKey].time = U16_to_F32(time_short,0.0,anim->duration);
						uint16_t rot_short;
						read(data_U16,&rot_short,"\t\trot_x",fp,type);
						anim->joints[indexJoint].rots[indexRotKey].rot_x = U16_to_F32(rot_short,-1.0,1.0);
						read(data_U16,&rot_short,"\t\trot_y",fp,type);
						anim->joints[indexJoint].rots[indexRotKey].rot_y = U16_to_F32(rot_short,-1.0,1.0);
						read(data_U16,&rot_short,"\t\trot_z",fp,type);
						anim->joints[indexJoint].rots[indexRotKey].rot_z = U16_to_F32(rot_short,-1.0,1.0);
					}
					break;
					case file_txtanim:
					for(int indexRotKey=0; indexRotKey<anim->joints[indexJoint].n_rot_keys ; ++indexRotKey)
					{	
						read(data_F32,&anim->joints[indexJoint].rots[indexRotKey].time,"\t\ttime",fp,type);

						read(data_F32,&anim->joints[indexJoint].rots[indexRotKey].rot_x,"\t\trot_x",fp,type);
						read(data_F32,&anim->joints[indexJoint].rots[indexRotKey].rot_y,"\t\trot_y",fp,type);
						read(data_F32,&anim->joints[indexJoint].rots[indexRotKey].rot_z,"\t\trot_z",fp,type);
					}
					break;
				}
			
				read(data_S32, &anim->joints[indexJoint].n_pos_keys, "\tnum_pos_keys",fp,type);
				#ifdef VERBOSE
				if(VERBOSE) printf("%d\n",anim->joints[indexJoint].n_pos_keys);
				#endif
				// reserve memory for posns
				anim->joints[indexJoint].posns = malloc(anim->joints[indexJoint].n_pos_keys*sizeof(struct pos_data)) ;
				for(int indexPosKey=0; indexPosKey<(anim->joints[indexJoint].n_pos_keys) ; ++indexPosKey)
				{	
					//
					// will need to load differently for anim and txtanim.
					//
					switch(type)
					{	case file_anim:
							uint16_t time_short;
							read(data_U16,&time_short,"\t\ttime",fp,type);
							anim->joints[indexJoint].posns[indexPosKey].time = U16_to_F32(time_short,0.0,anim->duration);
							uint16_t pos_short;
							read(data_U16,&pos_short,"\t\tpos_x",fp,type);
							anim->joints[indexJoint].posns[indexPosKey].pos_x = U16_to_F32(pos_short,-5.0,5.0);
							read(data_U16,&pos_short,"\t\tpos_y",fp,type);
							anim->joints[indexJoint].posns[indexPosKey].pos_y = U16_to_F32(pos_short,-5.0,5.0);
							read(data_U16,&pos_short,"\t\tpos_z",fp,type);
							anim->joints[indexJoint].posns[indexPosKey].pos_z = U16_to_F32(pos_short,-5.0,5.0);
						break;
						case file_txtanim:
							read(data_F32,&anim->joints[indexJoint].posns[indexPosKey].time,"\t\ttime",fp,type);

							read(data_F32,&anim->joints[indexJoint].posns[indexPosKey].pos_x,"\t\tpos_x",fp,type);
							read(data_F32,&anim->joints[indexJoint].posns[indexPosKey].pos_y,"\t\tpos_y",fp,type);
							read(data_F32,&anim->joints[indexJoint].posns[indexPosKey].pos_z,"\t\tpos_z",fp,type);
						break;
					}
				}
			}
			read(data_U32,&anim->n_constraints,"num_contraints",fp,type);
			anim->constraints = malloc(anim->n_constraints*sizeof(struct constraint_data));
			for(int indexConstraint=0; indexConstraint<anim->n_constraints; ++indexConstraint)
			{
				read(data_U8,&anim->constraints[indexConstraint].chain_length,"\t\tchain_length",fp,type);
				read(data_U8,&anim->constraints[indexConstraint].constraint_type,"\tconstraint_type",fp,type);
				// no need to malloc string16 because using a preallocated array.
				read(data_String16,anim->constraints[indexConstraint].source_volume,"\tsource_volume",fp,type);
				read(data_V3,&anim->constraints[indexConstraint].source_offset,"\tsource_offset",fp,type);

				read(data_String16,anim->constraints[indexConstraint].target_volume,"\ttarget_volume",fp,type);
				read(data_V3,&anim->constraints[indexConstraint].target_offset,"\ttarget_offset",fp,type);
				read(data_V3,&anim->constraints[indexConstraint].target_dir,"\ttarget_dir",fp,type);

				read(data_F32,&anim->constraints[indexConstraint].ease_in_start,"\tease_in_start",fp,type);
				read(data_F32,&anim->constraints[indexConstraint].ease_in_stop,"\tease_in_stop",fp,type);
				read(data_F32,&anim->constraints[indexConstraint].ease_out_start,"\tease_out_start",fp,type);
				read(data_F32,&anim->constraints[indexConstraint].ease_out_stop,"\tease_out_stop",fp,type);
			}
			
		break;
		case file_bvh:
			// initialize header with defaults.
			anim->version = 1;
			anim->subversion = 0;
			//anim->priority = 4;
			printf("Animation priority: ");
			scanf("%d", &anim->priority);

			anim->duration = 0.0;
			anim->name = "";
			anim->loop_in =  0.0;
			anim->loop_out = 0.0;
			//anim->loop = 0;
			printf("Does the animation loop? (y or n) ");
			{ // scope 'loop' here.
			char loop;
			while(1){
				scanf(" %c",&loop);
				switch(loop)
				{	case 'y': case 'Y':
					anim->loop=1;
					goto yes_loop; // we need a goto to double break,
					// and as long as we're using goto, might as well nest in an implied if statement.
					case 'n': case 'N':
					anim->loop=0;
					goto no_loop;
					default:
					printf("\nYes or No? ");
				}
			} }
			yes_loop:
			printf("loop_in time: ");
			scanf("%f", &anim->loop_in);

			printf("loop_out time: ");
			scanf("%f", &anim->loop_out);
			no_loop:

			//anim->ease_in = 0.2;
			printf("ease_in amount: ");
			scanf("%f", &anim->ease_in);
			//anim->ease_out= 0.2;
			printf("ease_out amount: ");
			scanf("%f", &anim->ease_out);
			anim->hand_pose = 1;

			anim->n_joints = 0; // will increment as joints are read.

			// dynamic allocation is hard, so just allocate max possible usage.
			anim->joints = malloc(216*sizeof(struct joint_data));
			// array of pointers to pointers to floats.
			// assume position and rotation for all joints = 6*216 = 1296.
			struct target_spec
			{	int joint;
				enum axis_spec axis;
			} targets[1296]; // spots to read data into.
			enum axis_spec conversion[1296];
			
			int nTargets;

			char buffer[32];
			{ // scope these variables to the while loop.
			int index_joint = -1; // will increment upon reading ROOT.
			int index_target = -1; // will increment on reading a target.
			while(1)
			{	// read in joint names and channels. + other HEIRARCHY data.
				fscanf(fp,"%s ",&buffer);
				if(!strcmp(buffer,"ROOT") || !strcmp(buffer,"JOINT"))
				{	//fscanf(fp,"%s ",anim->joints[++index_joint].name=malloc(32));
					fscanf(fp,"%s ",buffer);
					anim->joints[++index_joint].name = bone2bone(buffer);

					anim->joints[index_joint].n_pos_keys=0; 
					++anim->n_joints;
				}else if(!strcmp(buffer,"CHANNELS"))
				{	int nChannels;
					fscanf(fp,"%i ",&nChannels);
					while(nChannels--)
					{	fscanf(fp,"%s ",buffer);
						++index_target;
						++nTargets;
						targets[index_target].joint = index_joint;
						if(!strcmp(buffer,"Xposition"))
						{	targets[index_target].axis = m_X;
							anim->joints[index_joint].n_pos_keys=1; 
						}else if(!strcmp(buffer,"Yposition"))
						{	targets[index_target].axis = m_Y;
						}else if(!strcmp(buffer,"Zposition"))
						{	targets[index_target].axis = m_Z;
						}else if(!strcmp(buffer,"Xrotation"))
						{	targets[index_target].axis = a_X;
						}else if(!strcmp(buffer,"Yrotation"))
						{	targets[index_target].axis = a_Y;
						}else if(!strcmp(buffer,"Zrotation"))
						{	targets[index_target].axis = a_Z;
						}else
						{	printf("WARNING! %s is unknown channel spec.",buffer);
						}
					}
				}else if(!strcmp(buffer,"MOTION"))
				{	break;
				}
			} }
			// read these.
			int nFrames;
			fscanf(fp,"%*s %d",&nFrames);
			printf("nFrames: %d\n",nFrames);
			float frame_time;
			fscanf(fp,"%*s %*s %f",&frame_time);
			printf("Frame time: %f\n",frame_time);
			anim->duration = nFrames*frame_time;

			// malloc storage, set all times.
			for(int index_joint=0;index_joint<anim->n_joints;++index_joint)
			{	anim->joints[index_joint].priority = anim->priority;
				anim->joints[index_joint].n_rot_keys = nFrames;
				anim->joints[index_joint].rots = malloc(nFrames*sizeof(struct rot_data));
				if(anim->joints[index_joint].n_pos_keys)
				{	anim->joints[index_joint].n_pos_keys = nFrames; 
					anim->joints[index_joint].posns = malloc(nFrames*sizeof(struct pos_data));
					for(int index_t=0;index_t<nFrames;++index_t)
					{	anim->joints[index_joint].posns[index_t].time = index_t*frame_time;
					}
				}
				for(int index_t=0;index_t<nFrames;++index_t)
				{	anim->joints[index_joint].rots[index_t].time = index_t*frame_time;
				}
			}

			// read in all data.
			for(int index_frame=0;index_frame<nFrames;++index_frame)
			{	for(int index_target=0;index_target<nTargets;index_target+=3)
				{	int index_joint = targets[index_target].joint;
					float data[3];
					fscanf(fp,"%f %f %f",data,data+1,data+2);
					switch(targets[index_target].axis)
					{	case a_X:
						case a_Y:
						case a_Z:
						struct quat r = euler2rot(data[0],data[1],data[2],
							targets[index_target].axis,
							targets[index_target+1].axis,
							targets[index_target+2].axis);
						//printf("Converted euler to quat\n");
						//printf("  Euler: %f %f %f\n",data[0],data[1],data[2]);
						//printf("  Axes:  %d %d %d\n",
						//	targets[index_target].axis,
						//	targets[index_target+1].axis,
						//	targets[index_target+2].axis);
						//printf("  Quat:  %f %f %f %f\n",r.x,r.y,r.z,r.s);
						anim->joints[index_joint].rots[index_frame].rot_x=r.x;
						anim->joints[index_joint].rots[index_frame].rot_y=r.y;
						anim->joints[index_joint].rots[index_frame].rot_z=r.z;
						break;
						case m_X:
						case m_Y:
						case m_Z:

						// convert inches to meters using the same constant used by LL:
						data[0]*= 0.02540005f;
						data[1]*= 0.02540005f;
						data[2]*= 0.02540005f;

						// yes, this is a bit redundant.
						switch(targets[index_target].axis)
						{	// axes switch from BVH to linden.
							case m_X:
							anim->joints[index_joint].posns[index_frame].pos_y=data[0];
							break;
							case m_Y:
							anim->joints[index_joint].posns[index_frame].pos_z=data[0];
							break;
							case m_Z:
							anim->joints[index_joint].posns[index_frame].pos_x=data[0];
							break;
							default:
							printf("Warning: Misaligned data read!!!");
							
						}
						switch(targets[index_target+1].axis)
						{	// axes switch from BVH to linden.
							case m_X:
							anim->joints[index_joint].posns[index_frame].pos_y=data[1];
							break;
							case m_Y:
							anim->joints[index_joint].posns[index_frame].pos_z=data[1];
							break;
							case m_Z:
							anim->joints[index_joint].posns[index_frame].pos_x=data[1];
							break;
							default:
							printf("Warning: Misaligned data read!!!");
						}
						switch(targets[index_target+2].axis)
						{	// axes switch from BVH to linden.
							case m_X:
							anim->joints[index_joint].posns[index_frame].pos_y=data[2];
							break;
							case m_Y:
							anim->joints[index_joint].posns[index_frame].pos_z=data[2];
							break;
							case m_Z:
							anim->joints[index_joint].posns[index_frame].pos_x=data[2];
							break;
							default:
							printf("Warning: Misaligned data read!!!");
						}
					}
				}
			}
			
	}

	return 1;
}
int write_out_anim(struct anim* anim,FILE* fp,enum Filetype type)
{
	switch(type)
	{
		case file_anim:
		case file_txtanim:
			write(data_U16,&anim->version,"version",fp,type);
			write(data_U16,&anim->subversion,"subversion",fp,type);
				// check that version ==1, subversion == 0

			write(data_S32,&anim->priority,"base_priority",fp,type);
			write(data_F32,&anim->duration,"duration",fp,type);
				// check against maximum duration.
			write(data_String,anim->name,"emote_name",fp,type);
			write(data_F32,&anim->loop_in,"loop_in_point",fp,type);
			write(data_F32,&anim->loop_out,"loop_out_point",fp,type);
			write(data_S32,&anim->loop,"loop?",fp,type);
			write(data_F32,&anim->ease_in,"ease_in_duration",fp,type);
			write(data_F32,&anim->ease_out,"ease_out_duration",fp,type);
			write(data_U32,&anim->hand_pose,"hand_pose",fp,type);
			write(data_U32,&anim->n_joints,"num_joints",fp,type);
			for(int indexJoint=0; indexJoint<anim->n_joints; ++indexJoint)
			{
				write(data_String, anim->joints[indexJoint].name, "\tjoint_name",fp,type);
				write(data_S32, &anim->joints[indexJoint].priority, "\tjoint_priority",fp,type);
				write(data_S32, &anim->joints[indexJoint].n_rot_keys, "\tnum_rot_keys",fp,type);
				switch(type) // would be more efficient to have alternate for loops, but meh.
				{
					case file_txtanim:
					for(int indexRotKey=0; indexRotKey<anim->joints[indexJoint].n_rot_keys ; ++indexRotKey)
					{	
						write(data_F32,&anim->joints[indexJoint].rots[indexRotKey].time,"\t\ttime",fp,type);
						write(data_F32,&anim->joints[indexJoint].rots[indexRotKey].rot_x,"\t\trot_x",fp,type);
						write(data_F32,&anim->joints[indexJoint].rots[indexRotKey].rot_y,"\t\trot_y",fp,type);
						write(data_F32,&anim->joints[indexJoint].rots[indexRotKey].rot_z,"\t\trot_z",fp,type);
					}
					break;
					case file_anim:
					for(int indexRotKey=0; indexRotKey<anim->joints[indexJoint].n_rot_keys ; ++indexRotKey)
					{	
						uint16_t out = F32_to_U16(anim->joints[indexJoint].rots[indexRotKey].time,0.0,anim->duration);
						write(data_U16,&out,"\t\ttime",fp,type);

						out = F32_to_U16(anim->joints[indexJoint].rots[indexRotKey].rot_x,-1.0,1.0);
						write(data_U16,&out,"\t\trot_x",fp,type);
						out = F32_to_U16(anim->joints[indexJoint].rots[indexRotKey].rot_y,-1.0,1.0);
						write(data_U16,&out,"\t\trot_y",fp,type);
						out = F32_to_U16(anim->joints[indexJoint].rots[indexRotKey].rot_z,-1.0,1.0);
						write(data_U16,&out,"\t\trot_z",fp,type);
					}
				}
				write(data_S32, &anim->joints[indexJoint].n_pos_keys, "\tnum_pos_keys",fp,type);
				switch(type)
				{
					case file_txtanim:
					for(int indexPosKey=0; indexPosKey<anim->joints[indexJoint].n_pos_keys ; ++indexPosKey)
					{	
						write(data_F32,&anim->joints[indexJoint].posns[indexPosKey].time,"\t\ttime",fp,type);
						write(data_F32,&anim->joints[indexJoint].posns[indexPosKey].pos_x,"\t\tpos_x",fp,type);
						write(data_F32,&anim->joints[indexJoint].posns[indexPosKey].pos_y,"\t\tpos_y",fp,type);
						write(data_F32,&anim->joints[indexJoint].posns[indexPosKey].pos_z,"\t\tpos_z",fp,type);
					}
					break;
					case file_anim:
					for(int indexPosKey=0; indexPosKey<anim->joints[indexJoint].n_pos_keys ; ++indexPosKey)
					{	
						uint16_t out = F32_to_U16(anim->joints[indexJoint].posns[indexPosKey].time,0.0,anim->duration);
						write(data_U16,&out,"\t\ttime",fp,type);

						out = F32_to_U16(anim->joints[indexJoint].posns[indexPosKey].pos_x,-5.0,5.0);
						write(data_U16,&out,"\t\tpos_x",fp,type);
						out = F32_to_U16(anim->joints[indexJoint].posns[indexPosKey].pos_y,-5.0,5.0);
						write(data_U16,&out,"\t\tpos_y",fp,type);
						out = F32_to_U16(anim->joints[indexJoint].posns[indexPosKey].pos_z,-5.0,5.0);
						write(data_U16,&out,"\t\tpos_z",fp,type);
					}
				}
			}
			write(data_U32,&anim->n_constraints,"num_contraints",fp,type);
			for(int indexConstraint=0; indexConstraint<anim->n_constraints; ++indexConstraint)
			{
				write(data_U8,&anim->constraints[indexConstraint].chain_length,"\t\tchain_length",fp,type);
				write(data_U8,&anim->constraints[indexConstraint].constraint_type,"\tconstraint_type",fp,type);
				write(data_String16,anim->constraints[indexConstraint].source_volume,"\tsource_volume",fp,type);
				write(data_V3,&anim->constraints[indexConstraint].source_offset,"\tsource_offset",fp,type);
				write(data_String16,anim->constraints[indexConstraint].target_volume,"\ttarget_volume",fp,type);
				write(data_V3,&anim->constraints[indexConstraint].target_offset,"\ttarget_offset",fp,type);
				write(data_V3,&anim->constraints[indexConstraint].target_dir,"\ttarget_dir",fp,type);
				write(data_F32,&anim->constraints[indexConstraint].ease_in_start,"\tease_in_start",fp,type);
				write(data_F32,&anim->constraints[indexConstraint].ease_in_stop,"\tease_in_stop",fp,type);
				write(data_F32,&anim->constraints[indexConstraint].ease_out_start,"\tease_out_start",fp,type);
				write(data_F32,&anim->constraints[indexConstraint].ease_out_stop,"\tease_out_stop",fp,type);
			}
		break;
		case file_bvh:
			printf("bvh output unsupported.");
			return 0;
	}

	return 1;
}

int main(int argc, char *argv[])
{	
	enum Filetype typeIn;
	char* readMode;
	enum Filetype typeOut;
	char* writeMode;
	if(argc==3)
	{	// get input filetype:
		int ind_char =0;
		int ind_dot =0;
		do{	char c = argv[1][ind_char];
			if(c=='.') ind_dot = ind_char;
			if(c=='\0') break;
		}while(++ind_char);
		if(!strcmp(argv[1]+ind_dot,".anim"))
		{	typeIn = file_anim;
			readMode = "rb";
		}else if(!strcmp(argv[1]+ind_dot,".txtanim"))
		{	typeIn = file_txtanim;
			readMode = "r";
		}else if(!strcmp(argv[1]+ind_dot,".bvh"))
		{	typeIn = file_bvh;
			readMode = "r";
		}else
		{	printf("Unable to determine input filetype.");
			return 1;
		}
		// get output filetype
		ind_char =0;
		ind_dot = 0;
		do{	char c = argv[2][ind_char];
			if(c=='.') ind_dot=ind_char;
			if(c=='\0') break;
		}while(++ind_char);
		if(!strcmp(argv[2]+ind_dot,".anim"))
		{	typeOut = file_anim;
			writeMode = "wb";
		}else if(!strcmp(argv[2]+ind_dot,".txtanim"))
		{	typeOut = file_txtanim;
			writeMode = "w";
		}else if(!strcmp(argv[2]+ind_dot,".bvh"))
		{	typeOut = file_bvh;
			writeMode = "w";
		}else
		{	printf("Unable to determine output filetype.");
			return 2;
		}
		
	}else
	{	printf("Incorect number of arguments.");
		return 3;
	}	
	struct anim animData;

	FILE* fileIn = fopen(argv[1], readMode);
	FILE* fileOut = fopen(argv[2], writeMode);
	if(!fileIn)
	{	printf("Unable to read In-file.");
		return 1;
	}
	if(!fileOut)
	{	printf("Unable to open Out-file.");
		return 2;
	}

	read_in_anim(&animData,fileIn,typeIn);
	write_out_anim(&animData,fileOut,typeOut);

	fclose(fileIn);
	fclose(fileOut);

	return 0;
	
}
