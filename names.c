#include <string.h>
#include <stdlib.h>
//int strcmp(const char *s1, const char *s2);

char* bone2bone(const char* bone)
{	//aliases transcribed from indra/newview/avatar_skeleton.xml
	if(!strcmp(bone,"hip"))
	{	return "mPelvis";
	}else if(!strcmp(bone,"abdomen"))
	{	return "mTorso";
	}else if(!strcmp(bone,"chest"))
	{	return "mChest";
	}else if(!strcmp(bone,"neck"))
	{	return "mNeck";
	}else if(!strcmp(bone,"head"))
	{	return "mHead";
	}else if(!strcmp(bone,"figureHair"))
	{	return "mSkull";

	}else if(!strcmp(bone,"lCollar")) // eye aliases have avatar_ prefix.
	{	return "mCollarLeft";
	}else if(!strcmp(bone,"lShldr"))
	{	return "mShoulderLeft";
	}else if(!strcmp(bone,"lForeArm"))
	{	return "mElbowLeft";
	}else if(!strcmp(bone,"lHand"))
	{	return "mWristLeft";

	}else if(!strcmp(bone,"rCollar")) // eye aliases have avatar_ prefix.
	{	return "mCollarRight";
	}else if(!strcmp(bone,"rShldr"))
	{	return "mShoulderRight";
	}else if(!strcmp(bone,"rForeArm"))
	{	return "mElbowRight";
	}else if(!strcmp(bone,"rHand"))
	{	return "mWristRight";

	}else if(!strcmp(bone,"rThigh"))
	{	return "mHipRight";
	}else if(!strcmp(bone,"rShin"))
	{	return "mKneeRight";
	}else if(!strcmp(bone,"rFoot"))
	{	return "mAnkleRight"; // foot and toe have avatar_ prefix.

	}else if(!strcmp(bone,"lThigh"))
	{	return "mHipLeft";
	}else if(!strcmp(bone,"lShin"))
	{	return "mKneeLeft";
	}else if(!strcmp(bone,"lFoot"))
	{	return "mAnkleLeft"; // foot and toe have avatar_ prefix.
	}else
	{	
	//TODO: if the first characters are avatar_ then return the string after that point.
		
	// input gets deallocated so:
	char* ret = malloc(32); 
	strcpy(ret,bone);
		return ret;
	}
}

/*
struct vector3
{	float x;
	float y;
	float z;
}

struct vector3 bone2rot(const char* bone)
{	
	struct vector3 ret;
	ret.x = 0;
	ret.y = 0;
	ret.z = 0;
	if(!strcmp(bone,"PELVIS"))
	{	ret.y = 8.0;
	}else if(!strcmp(bone,"BELLY"))
	{	ret.y = 8.0;
	}else if(!strcmp(bone,"CHEST"))
	{	ret.y = -10;
	}else if(!strcmp(bone,"LEFT_PEC"))
	{	ret.y = 4.29;
	}else if(!strcmp(bone,"RIGHT_PEC"))
	{	ret.y = 4.29
	}
 	 // . . . none of the aliased bones have non-zero rotation.
	return ret;
}
*/
