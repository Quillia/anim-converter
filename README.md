# Anim-converter
Convert between animation file formats for use on Secondlife(tm) and other virtual world platforms.

# Usage
\<binary filename\> inFile.format outFile.format

Currently supported input formats (case-sensitive) :
* anim
* txtanim
* bvh

output formats (also case-sensitive) :
* anim
* txtanim

The txtanim file format is for convenience in manually applying changes to the header or per-joint priorities; it is subject to changes and tweaks with later versions of this program.

## License
Copyright (C) 2022

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 

http://www.gnu.org/licenses/gpl-3.0.txt
